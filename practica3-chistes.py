#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False

    def startElement(self, name, attrs):

        if name == 'chiste':
            self.calificacion = attrs.get('calificacion', "")
            print(self.calificacion)
        elif name == 'pregunta':
            self.inPregunta = True
        elif name == 'respuesta':
            self.inRespuesta = True


    def endElement(self, name):

        if name == 'pregunta':
            print(self.pregunta)
            self.pregunta = ""
            self.inPregunta = False
        if name == 'respuesta':
            print(self.respuesta)
            self.respuesta = ""
            self.inRespuesta = False

    def characters(self, char):

        if self.inPregunta:
            self.pregunta = self.pregunta + char
        if self.inRespuesta:
            self.respuesta += char


if __name__ == "__main__":

    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes2.xml'))
