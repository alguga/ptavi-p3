#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):

        self.list = []
        self.root_layout = ['width', 'height', 'background-color']
        self.region = ['id', 'top', 'bottom', 'left', 'right']
        self.img = ['src', 'region', 'begin', 'dur']
        self.audio = ['src', 'begin', 'dur']
        self.textstream = ['src', 'region']

    def _structure_tags(self, tag, attrs, attrsName):
        """Funcion interna para generar la estructura"""
        self.list.append({tag: {}})
        for i in attrsName:
            self.list[-1][tag][i] = attrs.get(i, "")

    def get_tags(self):
        return self.list

    def startElement(self, name, attrs):

        if name == 'root-layout':
            self._structure_tags('root-layout', attrs, self.root_layout)
        elif name == 'region':
            self._structure_tags('region', attrs, self.region)
        elif name == 'img':
            self._structure_tags('img', attrs, self.img)
        elif name == 'audio':
            self._structure_tags('audio', attrs, self.audio)
        elif name == 'textstream':
            self._structure_tags('textstream', attrs, self.textstream)
