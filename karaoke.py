from smallsmilhandler import SmallSMILHandler
from xml.sax import make_parser
import sys
import json
import urllib.request


class KaraokeLocal:

    def __init__(self, file):
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        try:
            parser.parse(open(file))
        except FileNotFoundError:
            sys.exit("Usage: python3 karaoke.py file.smil")
        self.list = cHandler.get_tags()

    def __str__(self):
        full_text = ""
        for i in self.list:
            tag = list(i.keys())[0]
            line = tag
            for key in i[tag]:
                if i[tag][key] != "":
                    line += '\t' + key + '="' + i[tag][key] + '"'
            line += '\n'
            full_text += line
        return full_text

    def do_local(self):
        for tags in self.list:
            tag = list(tags.keys())[0]
            for key in tags[tag]:
                url = tags[tag][key]
                if key == "src" and url[0:7] == "http://":
                    urllib.request.urlretrieve(url, url.split("/")[-1])
                    tags[tag][key] = url.split("/")[-1]

    def to_json(self, file1, file2=""):

        if file2 == "":
            file2 = file1.split(".")[0] + ".json"
        with open(file2, 'w') as outfile:
            json.dump(self.list, outfile, indent=2)


if __name__ == "__main__":
    try:
        file_name = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 karaoke.py file.smil")
    karaoke = KaraokeLocal(file_name)
    print(karaoke)
    karaoke.to_json(file_name)
    karaoke.do_local()
    karaoke.to_json(file_name, "local.json")
    print(karaoke)
